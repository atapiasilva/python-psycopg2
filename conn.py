import psycopg2

#Paso1: crear conexión

conn = psycopg2.connect(
    host = "localhost",
    dbname = "pydb",
    user = "ltapia",
    password = 123456,
    port = 5432
)

#Paso2: crear cursor
cursor = conn.cursor() 

cursor.execute("DROP TABLE IF EXISTS STUDENT")

sql =   """ CREATE TABLE STUDENT(
            NAME CHAR (100) NOT NULL,
            LAST_NAME CHAR(100) NOT NULL,
            MOTHER_LAST_NAME CHAR (100) NOT NULL,
            AGE INT,
            GENDER CHAR(1)
            )
        """
cursor.execute(sql)

conn.commit()

print('Conexión exitosa')


#Paso final
conn.close()